/*
 * Copyright (C) 2022 Huawei Technologies Co., Ltd.
 * Licensed under the Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *     http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 * PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
#ifndef SE_STATUS_H
#define SE_STATUS_H
#include <stdint.h>

void sre_se_channel_info_write(uint32_t reader_id, uint32_t channel_id, uint32_t task_id);
void sre_se_channel_info_read(uint32_t reader_id, uint32_t *task_id, uint32_t *cnt);
void sre_se_deactive_write(uint32_t deactive);
void sre_se_deactive_read(uint32_t *deactive);
void sre_seaid_switch_write(uint8_t *seaid_list, uint32_t seaid_list_len);
void sre_seaid_list_len_read(uint32_t *seaid_list_len);
void sre_seaid_switch_read(uint8_t *seaid_list, uint32_t seaid_list_len);
void sre_se_connect_info_write(uint32_t reader_id, uint8_t *se_connect_info, uint32_t se_connect_info_len);
void sre_se_connect_info_read(uint32_t reader_id, uint8_t *se_connect_info, uint32_t *se_connect_info_len);

#endif
