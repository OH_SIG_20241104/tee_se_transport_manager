/*
 * Copyright (C) 2022 Huawei Technologies Co., Ltd.
 * Licensed under the Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *     http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 * PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
#ifndef SESRV_PERMISSION_CONFIG_H
#define SESRV_PERMISSION_CONFIG_H

#include "tee_defines.h"

TEE_Result se_service_check_msp_permission(const TEE_UUID *uuid);
bool is_msp_enable(void);
bool is_sec_flash_enable(void);
uint32_t get_vote_id(uint32_t reader_id, const TEE_UUID *uuid);

#endif
